use base "installedtest";
use strict;
use testapi;
use utils;

# This test tests that Settings starts
#
sub run {
    my $self = shift;

    # Start the application
    start_with_launcher('apps_menu_settings');

    # Check that it started
    assert_screen 'apps_run_settings';
    # Select Appearance menu item
    assert_and_click 'apps_settings_menu_appearance';
    wait_still_screen 10;
    save_screenshot;
    # Register application
    register_application("gnome-control-center");
    # Close the application
    quit_with_shortcut();
}

sub test_flags {
    return {always_rollback => 1};
}

1;

# vim: set sw=4 et:
