use base "installedtest";
use strict;
use testapi;
use utils;

my $dump_boot_configuration = 0;    # Set to 1 to dump the boot configuration

sub run {
    my $self     = shift;
    my $password = get_var("USER_PASSWORD", "weakpassword");
    my $version  = get_var("VERSION");
    # If KICKSTART is set, then the wait_time needs to consider the
    # install time. if UPGRADE, we have to wait for the entire upgrade
    # unless ENCRYPT_PASSWORD is set (in which case the postinstall
    # test does the waiting)
    my $wait_time = 300;
    $wait_time = 1800 if (get_var("KICKSTART"));
    $wait_time = 6000 if (get_var("UPGRADE") && !get_var("ENCRYPT_PASSWORD"));
    my $desktop = get_var('DESKTOP');

    if ($dump_boot_configuration) {
        my $bootlive = get_var('BOOT_LIVE',         'unset');
        my $bootfrom = get_var('BOOTFROM',          'unset');
        my $islive   = get_var('IS_LIVE',           'unset');
        my $uname    = get_var('USER_LOGIN',        'unset');
        my $upass    = get_var('USER_PASSWORD',     'unset');
        my $auto     = get_var('CONSOLE_AUTOLOGIN', 'unset');

        record_info('bootinfo', "BOOT_LIVE: $bootlive BOOTFROM: $bootfrom IS_LIVE: $islive USER_LOGIN: $uname USER_PASSWORD: $upass CONSOLE_AUTOLOGIN: $auto", result => 'ok');
    }

    # handle bootloader, if requested
    if (get_var("GRUB_POSTINSTALL")) {
        do_bootloader(postinstall => 1, params => get_var("GRUB_POSTINSTALL"), timeout => $wait_time);
        $wait_time = 300;
    } elsif (get_var('BOOT_LIVE')) {
        do_bootloader(live => 1, timeout => $wait_time);
    }

    # Apply a sane default value for DM_NEEDS_USERNAME if it wasn't set before
    my $dm_needs_username = get_var('DM_NEEDS_USERNAME');
    if (!defined($dm_needs_username)) {
        $dm_needs_username = (!defined($desktop) ||    # text mode
              $desktop eq 'cinnamon' ||
              $desktop eq 'mate'     ||
              $desktop eq 'lxde'     ||
              $desktop eq 'lxqt'     ||
              $desktop eq 'xfce'     ||
              $desktop eq 'kali');
    }

    # Apply a sane default value for DM_REMEMBERS_USERNAME if it wasn't set before
    my $dm_remembers_username = get_var('DM_REMEMBERS_USERNAME');
    if (!defined($dm_remembers_username)) {
        $dm_remembers_username = ($desktop eq 'lxqt');
    }

    # Apply a sane default value for SELECT_PASSWORD_KEYSTROKE if it wasn't set before
    my $dm_select_password_keystroke = get_var('SELECT_PASSWORD_KEYSTROKE', 'unset');
    if ($dm_select_password_keystroke eq 'unset') {
        $dm_select_password_keystroke = 'tab';
    }

    # Wait for the login screen, unless we're booting into the live
    # environment, which boots directly to the desktop
    unless (get_var('BOOT_LIVE')) {
        my $user_login = get_var("USER_LOGIN", "testy");
        boot_to_login_screen(timeout => $wait_time);
        # if USER_LOGIN is set to string 'false', we're done here
        return if ($user_login eq "false");

        # GDM 3.24.1 dumps a cursor in the middle of the screen here...
        mouse_hide;
        if (get_var("DESKTOP") eq 'gnome' ||
            get_var("DESKTOP") eq 'gnome_flashback') {
            # we have to hit enter to get the password dialog, and it
            # doesn't always work for some reason so just try it three
            # times
            send_key_until_needlematch("graphical_login_input", "ret", 3, 5);
        }
        # be ready to retype the password, as it sometimes fails
        # sadly, this doesn't help with mis-typed usernames.
        for my $try (0 .. 3) {
            if ((0 == $try) && $dm_needs_username) {
                if ($dm_remembers_username) {
                    # graphical_login_username: Username is not provided, focus field is the username
                    # graphical_login_input: Username has been entered, focus field is the password
                    assert_screen ['graphical_login_username', 'graphical_login_input'], 90;
                    if (match_has_tag('graphical_login_username')) {
                        type_very_safely $user_login;
                        send_key $dm_select_password_keystroke;
                    }
                } else
                {
                    assert_screen 'graphical_login_username', 90;
                    type_very_safely $user_login;
                    send_key $dm_select_password_keystroke;
                }
            }
            assert_screen "graphical_login_input";

            my $pw_maybe = $password;
            $pw_maybe .= "X" if (0 == $try);    # break the password on the first pass
            if (get_var("SWITCHED_LAYOUT")) {
                # see _do_install_and_reboot; when layout is switched
                # user password is doubled to contain both US and native
                # chars
                desktop_switch_layout 'ascii';
                type_string $pw_maybe;
                desktop_switch_layout 'native';
                type_string $pw_maybe;
            }
            else {
                type_string $pw_maybe;
            }
            wait_still_screen(stilltime => 2, similarity_level => 35);
            save_screenshot;
            wait_screen_change { send_key "ret"; };

            if ($try == 0) {
                # this is a feeble attempt to catch the gdm login failure, as it flits past
                my $failure_seen = 0;
                foreach (1 .. 5) {
                    if (check_screen "authentication_failed", 2) {
                        $failure_seen = 1;
                        last;
                    }
                }
                die "first login attempt should fail, with invalid password"
                  unless $failure_seen;
            } else {
                if (check_screen "authentication_failed", 5) {
                    # expect initial failure, but record a soft failure otherwise (to notice typo issues)
                    record_soft_failure "Authentication failed (try: $try)";
                } else {
                    last;
                }
            }
        }
    }

    # For GNOME, handle initial-setup or welcome tour, unless START_AFTER_TEST
    # is set in which case it will have been done already. Always
    # do it if ADVISORY_OR_TASK is set, as for the update testing flow,
    # START_AFTER_TEST is set but a no-op and this hasn't happened
    if (get_var("DESKTOP") eq 'gnome'
        && (get_var("ADVISORY_OR_TASK")
            || !get_var("START_AFTER_TEST"))) {
        # as this test gets loaded twice on the ADVISORY_OR_TASK flow,
        # check whether this happened already
        gnome_initial_setup() unless (get_var("_SETUP_DONE"));
    }

    # Move the mouse somewhere it won't highlight the match areas
    mouse_set(300, 800);
    # KDE can take ages to start up
    check_desktop(timeout => (check_var('DESKTOP', 'kde') ? 200 : 150));
}

sub test_flags {
    my %ret = (fatal => 1);

    # FIXME: we should decide on some criteria that trigger
    # the need for a milestone here, in the meantime we have a variable that's
    # not really intended to be defined
    $ret{milestone} = 1 if get_var('GRAPHICAL_WAIT_LOGIN_MILESTONE');
    return \%ret;
}

1;

# vim: set sw=4 et:
