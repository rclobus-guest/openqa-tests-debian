# Debian's openQA tests
#
# Copyright 2022 Philip Hands, Roland Clobus
#
# This script needs to be provided with parameters:
# menu_path: the path through the menu
# needle: (optional) the destination needle to look for
#         when not provided, the menu_path should not be reachable
#
# Debug options:
#  skip_installer_items

use base 'basetest';
use strict;
use warnings;
use testapi;
use utils;

my $skip_installer_items = 0;    # Set to 1 to skip the installer menu items

sub my_send_key {
    my ($key, $wait_screen_change) = @_;
    if ($key eq 'ret') {
        wait_still_screen 3;
        save_screenshot;
        send_key $key, wait_screen_change => 1;

        # Work around for #1070006
        wait_still_screen 3;
        if (check_screen('boot_no_video_mode_activated')) {
            record_soft_failure('No video mode activated, deb#1070006');
            send_key 'spc', wait_screen_change => 1;
            wait_still_screen 3;
            save_screenshot;
        }
    } else {
        if ($wait_screen_change) {
            send_key $key, wait_screen_change => 1;
        } else {
            send_key $key;
        }
    }
}

sub run {
    my ($self, $run_args) = @_;
    die 'Need menu_path to know which test to run' unless $run_args && $run_args->{menu_path};
    my @menu_path = @{$run_args->{menu_path}};
    my $needle    = $run_args->{needle};

    power('reset');

    # Wait for the boot screen, since we start each of these tests from scratch
    my $boottag = 'bootloader_bios';
    $boottag = 'bootloader_uefi' if (get_var('UEFI'));
    assert_screen $boottag, 25;    # short enough to avoid the speech-mode timeout
    if (match_has_tag 'bootloader_secure_uefi_verification_failed') {
        # A signed grub is not available, but the shim is
        # Enroll the key for grubx86_64.efi and try again
        send_key 'ret', wait_screen_change => 1;    # OK on Verification failed
        send_key 'm';                               # Any key to enter MOK Management
        wait_still_screen 1;
        send_key 'down', wait_screen_change => 1;
        send_key 'down', wait_screen_change => 1;
        wait_still_screen 1;
        save_screenshot;
        send_key 'ret';                             # Enroll hash from disk
        wait_still_screen 1;
        save_screenshot;
        send_key 'ret';
        wait_still_screen 1;
        save_screenshot;
        send_key 'ret';
        wait_still_screen 1;
        send_key 'down', wait_screen_change => 1;
        save_screenshot;
        send_key 'ret';
        wait_still_screen 1;
        send_key 'down', wait_screen_change => 1;
        send_key 'down', wait_screen_change => 1;
        save_screenshot;
        send_key 'ret';                # This is grubx64.efi
        wait_still_screen 1;
        send_key 'down', wait_screen_change => 1;
        save_screenshot;
        send_key 'ret';                # Enroll the key
        wait_still_screen 1;
        send_key 'down', wait_screen_change => 1;
        save_screenshot;
        send_key 'ret';                # Yes, I'm sure
        wait_still_screen 1;
        save_screenshot;
        send_key 'ret';                # Reboot
        assert_screen $boottag, 25;    # short enough to avoid the speech-mode timeout
    }
    my @key_presses = ();
    foreach my $downs (@menu_path) {
        foreach (1 .. $downs) {
            push @key_presses, 'down';
        }
        foreach (1 .. -$downs) {
            push @key_presses, 'up';
        }
        push @key_presses, 'ret';
    }

    # The last 'ret' keypress will be sent later
    pop @key_presses;

    # if there are no keys to press, send TAB ESC to halt the speech-install countdown
    @key_presses = qw/tab esc/ unless @key_presses;

    # Send all key presses, except for the last one
    while (@key_presses > 1) {
        my $key = shift @key_presses;
        my_send_key $key, wait_screen_change => 1;
    }

    # Send the last key press when everything has calmed down
    # Look for changes when the key has been pressed
    my $changed;
    if (@key_presses > 0) {
        wait_still_screen(stilltime => 5, timeout => 25, similarity_level => 45);
        my $key = shift @key_presses;
        $changed = wait_screen_change(
            sub {
                my_send_key $key;
            },
            5
        );
        # my_send_key waits for a still screen when the key is 'ret', so fake a change here
        $changed = 1 if ($key = 'ret' && defined $needle);
    }
    else {
        # The very first menu entry is selected
        $changed = 1;
    }

    if (defined $needle) {
        save_screenshot;
        if ($skip_installer_items && $needle =~ /^(debianinstaller|text|speech)_/)
        {
            return;
        }
        if ($changed) {
            start_audiocapture;
            my_send_key 'ret';
            assert_screen $needle, 180;
        }
        else {
            record_soft_failure("The screen did not change while waiting for ${needle}. "
                  . 'The test script needs to be updated.');
        }
    }
    else {
        record_soft_failure('The screen did change, but should not have changed. '
              . 'The test script needs to be updated.') if $changed;
    }
    return;
}

# there's not much point trying to collect post-fail data, so let's not
sub post_fail_hook {
}

1;
