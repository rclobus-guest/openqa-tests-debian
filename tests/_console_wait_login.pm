use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;
    # If UPGRADE is set, we have to wait for the entire upgrade
    my $wait_time = 300;
    $wait_time = 6000 if (get_var("UPGRADE"));

    # handle bootloader, if requested
    if (get_var("GRUB_POSTINSTALL")) {
        do_bootloader(postinstall => 1, params => get_var("GRUB_POSTINSTALL"), timeout => $wait_time);
        $wait_time = 240;
    } elsif (get_var('BOOT_LIVE')) {
        do_bootloader(live => 1, timeout => $wait_time);
    }

    # handle initial-setup, if we're expecting it (ARM disk image)
    if (get_var("INSTALL_NO_USER")) {
        console_initial_setup;
    }

    # Wait for the text login
    boot_to_login_screen(timeout => $wait_time);

    # Login as root
    $self->root_console;
}

sub test_flags {
    return {fatal => 1, milestone => 1};
}

1;

# vim: set sw=4 et:
