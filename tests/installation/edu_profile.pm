# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    assert_screen 'EduChooseProfile', 120;
    my $eduprofile = get_var('EDUPROFILE');

    if ($eduprofile eq 'standalone') {
        send_key 'spc';
        send_key 'down';
        send_key 'spc';
        send_key 'down';
        send_key 'down';
        send_key 'spc';
        send_key 'down';
        send_key 'spc';
    }

    assert_screen $eduprofile . '_selected' if $eduprofile;
    send_key 'tab'                          if ('gtk' eq get_var('DI_UI', 'gtk'));
    assert_screen_change { send_key 'ret' };
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
