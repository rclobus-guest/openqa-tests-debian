# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    # Note: there is a symlink for mirror_selection.pm that points at install_base.pm
    #       we should probably work out where to split these in two, to avoid this
    #       duplication, as it is bound to cause confusion later.  (FIXME)

    my @tags = qw(blankScreen popcon BadMirror MirrorCountry EnterInformationManually ScanCD useNetMirror rootPassword ChooseSoftware InstallGRUB EnterRescueMode KernelMismatch);
    my $nochange = 0;

    while ($nochange < 5) {

        if (wait_screen_change(sub { sleep 5; }, 55, similarity_level => 95)) {
            diag 'wait_screen_change: changed :-)';
            $nochange = 0;
        }
        else {
            $nochange++;
            diag "wait_screen_change: no change  :-( [ $nochange ]";
        }

        if (check_screen \@tags, 5) {
            if (match_has_tag("blankScreen")) {
                # FIXME -- I think this should be fixed, but let's allow some nice green results for now
                # record_soft_failure 'deb#787279: a screen-saver in the installer is mostly pointless';
                mouse_set(800, 800);
                sleep 1;
                mouse_hide;
            }
            elsif (match_has_tag('ScanCD')) {
                send_key 'ret';
                @tags = grep { ("ScanCD" ne $_) } @tags;
            }
            elsif (match_has_tag('useNetMirror')) {
                send_key 'down';
                send_key 'ret';
                @tags = grep { ("useNetMirror" ne $_) } @tags;
            }
            elsif (match_has_tag('popcon')) {
                send_key 'ret';

                # debian-edu automates most of this stuff
                return 1 if get_var('EDUPROFILE');
                return 1 if check_var('DISTRI', 'debian-edu');    # should be redundant now

                @tags = grep { ("popcon" ne $_) } @tags;
            }
            elsif (match_has_tag('MirrorCountry')
                || match_has_tag('EnterInformationManually')) {
                my $manual = match_has_tag('EnterInformationManually');
                send_key 'ret';
                assert_screen 'ArchiveMirror';
                send_key 'ret';
                if ($manual) {
                    assert_screen 'MirrorDirectory';
                    send_key 'ret';
                }
                assert_screen 'HttpProxy';
                my $http_proxy = get_var("HTTP_PROXY");
                if ("$http_proxy") {
                    type_string "$http_proxy";
                    wait_still_screen 2;
                    save_screenshot;
                }
                send_key 'ret';
                @tags = grep { (("MirrorCountry" ne $_)
                          && ("EnterInformationManually" ne $_)) } @tags;
            }
            elsif (match_has_tag('rootPassword')) {
                # the mini.iso does this in an odd order
                return 1;
            }
            elsif (match_has_tag('InstallGRUB')) {
                # kali does this in a different order
                return 1;
            }
            elsif (match_has_tag('EnterRescueMode')) {
                # if we're rescuing, it seems like we got there
                return 1;
            }
            elsif (match_has_tag('BadMirror')) {
                send_key 'ret';
                # die "Failed to access the mirror (perhaps a duff proxy?)";
            }
            elsif (match_has_tag('KernelMismatch')) {
                die "'KernelMismatch' (probably testing a mini-ISOs, which brings up the network earlier, and so sneaks past the normal kernel_mismatch check)";
            }
            else {
                if (check_var('DI_DESELECT_DESKTOP_HEADING', 1)) {
                    # deselect the Desktop Heading
                    send_key 'ret';
                }
                last;
            }
        }
    }

    assert_screen 'ChooseSoftware';
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
